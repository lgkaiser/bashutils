#!/usr/bin/env bash

#   extract:  Extract most know archives with one command
#   ---------------------------------------------------------
extract () {
    if [ -f $1 ] ; then
      case $1 in
        *.tar.bz2)   tar xjf $1     ;;
        *.tar.gz)    tar xzf $1     ;;
        *.bz2)       bunzip2 $1     ;;
        *.rar)       unrar e $1     ;;
        *.gz)        gunzip $1      ;;
        *.tar)       tar xf $1      ;;
        *.tbz2)      tar xjf $1     ;;
        *.tgz)       tar xzf $1     ;;
        *.zip)       unzip $1       ;;
        *.Z)         uncompress $1  ;;
        *.7z)        7z x $1        ;;
        *)     echo "'$1' cannot be extracted via extract()" ;;
         esac
     else
         echo "'$1' is not a valid file"
     fi
}

#   findPid: find out the pid of a specified process
#   -----------------------------------------------------
#       Note that the command name can be specified via a regex
#       E.g. findPid '/d$/' finds pids of all processes with names ending in 'd'
#       Without the 'sudo' it will only find processes of the current user
#   -----------------------------------------------------
findPid () { lsof -t -c "$@" ; }


#	----------------------------------------------------------------------------------------------
#	RELOAD SOURCE
#	----------------------------------------------------------------------------------------------
autosource(){
    source $BASHUTILSLOCATION/master.sh
}

#	----------------------------------------------------------------------------------------------
#	PRINTA TEXTO DE INFORMAÇÃO
#	----------------------------------------------------------------------------------------------
print_info(){
    printf '\e[37;40m%s\e[0m\n' "$1"
}

#	----------------------------------------------------------------------------------------------
#	PRINTA TEXTO DE AVISO
#	----------------------------------------------------------------------------------------------
print_warning(){
    printf '\e[33;40;1m%s\e[0m\n' "$1"
}

#	----------------------------------------------------------------------------------------------
#	PRINTA TEXTO DE PERIGO
#	----------------------------------------------------------------------------------------------
print_danger(){
    printf '\e[37;41;1m%s\e[0m\n' "$1"
}

#	----------------------------------------------------------------------------------------------
#	ALTERA O TÍTULO DA ABA
#	----------------------------------------------------------------------------------------------
setTabTitle(){
    echo -n -e "\033]0;- $1 -\007";
}

#	----------------------------------------------------------------------------------------------
#	MATA O PROCESSO DA VPN
#	----------------------------------------------------------------------------------------------
killVPN() {
    pid=`ps -eo pid,args | grep GlobalProtect | grep -v grep | cut -c1-6`; echo $pid;[[ "$pid" = "" ]] && echo "Não existe processo da VPN rodando" || { echo "Matando processo = ${pid}";sudo kill -9 $pid; };
}

# ----------------------------------------------------------------------------------------------
# COPIA UM VALOR PARA A ÁREA DE TRANSFERÊNCIA
# Exempo: copyToClipoboard Gpa@2016
# ----------------------------------------------------------------------------------------------
copyToClipoboard() { echo $1 | pbcopy; echo Valor copiado para área de transferência: $1; }
