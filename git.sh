#!/usr/bin/env bash

githelp(){
    print_info ""
    print_info "================================= GIT UTILITY ================================"
    print_info "|gitbranch          | Cria um branch"
    print_info "|gitcandidate       | Cria um branch candidato"
    print_info "|gitcheckout        | Troca de branch ou busca um branch no remote e trás para a máquina local"
    print_info "|gitisolate         | Isola um branch e apaga todos os outros"
    print_info "|gitmerge           | Realiza o merge de um branch"
    print_info "|gitpull            | Atualiza o branch com código do servidor"
    print_info "|gitpush            | Faz push para todos os repositórios"
    print_info "|gitremove          | Apaga os branches localmente"
    print_info "|gitremovebranches  | Apaga os branches locais e no servidor"
    print_info "|processGitLog   	| Lista os commit's realizados no branch atual após um commit de referência"    
    print_info "=============================================================================="
    print_info ""
}

# Apenas um atalho para o comando git pull
gitpull(){
    git fetch
    git pull
}

# Troca de branch ou busca um branch no remote e trás para a máquina local
# Parâmetros
# 1 - Nome do Branch
gitcheckout() {
    if [ $# -eq 1 ]; then
        branch=$1
        branchSearch=$(git branch | grep "^[ ]*$branch$")
        if [ ! -z ${branchSearch} ];then
            print_info "Branch existe localmente, fazendo a troca"
            git checkout ${branch}
        else
            print_info "Branch não existe localmente, trazendo do remote"
            git fetch
            repo=$(git remote | (head -n 1))
            git checkout -b ${branch} --track ${repo}/${branch}
        fi
    else
        print_info "Troca de branch ou busca um branch no remote e trás para a máquina local"
        print_info "Como usar:"
        print_warning "gitcheckout PAD_2016/PAD-4434"
    fi
}

# Cria um branch de tarefa
# Parâmetros
# 1 - PA ou Extra
# 2 - Nome do Branch
gitbranch() {
	if [ $# -eq 2 ]; then
        workingCopyStatus=$(git status --porcelain)
        if [ "$workingCopyStatus" != "" ]; then
            print_danger "Existem arquivos para serem comitados nesse branch, não é possível continuar."
            print_info ""
            print_info ${workingCopyStatus}
        else
            project=$1
            branchName=$2
            git checkout ${project}
            git pull
            branchName=$(echo ${project} | tr '[a-z]' '[A-Z]')_$(date +%Y)/${branchName}
            print_info "Criando branch $branchName"
            git branch ${branchName}
            print_info "Trocando para o branch $branchName"
            git checkout ${branchName}
        fi
    else
        if [ $# -eq 1 ]; then
            branchName=$1
            print_warning "Projeto base não determinado, escolha um (PA/Extra): "
			read answer
			if [ "$answer" == "PA" ] || [ "$answer" == "Extra" ] ;then
			    branchName=$(echo ${answer} | tr '[a-z]' '[A-Z]')_$(date +%Y)/${branchName}
			    print_info "Criando branch $branchName"
                git branch ${branchName}
                print_info "Trocando para o branch $branchName"
                git checkout ${branchName}
			else
			    print_danger "Ação cancelada"
			fi
        else
            print_info ""
            print_info "Cria um branch a partir do branch atual"
            print_info ""
            print_info "	Uso: gitbranch PA|Extra nome_do_branch"
            print_info ""
            print_info "	Uso: gitbranch nome_do_branch"
            print_info ""
        fi
    fi
}

# Cria um branch a partir do branch 
# Parâmetros
# 1 - PA ou Extra
# 2 - Sufixo (Nome após o nome do branch) (Opcional)
gitcandidate() {
	if [ $# -gt 0 ]
		then
			suffix=$2
			project=$1
			branchName=HLG_$(date +%m_%d)_$(git log --pretty=format:%h -n1)${suffix}
			print_warning "O branch criado terá o nome $(echo ${project} | tr '[a-z]' '[A-Z]')_$(date +%Y)/$branchName, continuar (y/n)? "
			read answer
			if echo "$answer" | grep -iq "^y" ;then
			    print_info "Criando branch $branchName"
			    print_info ""
			    git checkout ${project}
			    git pull
			    print_info "gitbranch $1 $branchName"
				gitbranch $1 ${branchName}
			else
			    print_danger "Ação cancelada"
			fi
		else
			print_info ""
			print_info "Cria um branch candidato partir do branch atual"
			print_info ""
			print_info "	Uso: gitbranch PA|Extra [sufixo]"
			print_info ""
			print_info "	sufixo -opcional: nome agregado ao nome do branch"
			print_info ""
		fi
}

# Realiza o merge de um branch
# Parâmetros
# 1 - Nome do branch
# 2 - Mensagem (Opcional)
gitmerge() {
	if [ $# -gt 0 ]
		then
			branchName="$1"
			message=$2
			if [ -z "$message" ]
				then
					message="Merge branch '${branchName}'' into $(git rev-parse --abbrev-ref HEAD)"
			fi

			for repo in $(git remote)
            do
                git merge --no-ff -m "$message" --log ${repo}/${branchName}
            done

            if [ ! -z $(git ls-files -u) ];then
                print_info ""
                print_danger "                                                               "
                print_danger "   ESSE MERGE GEROU UM CONFLITO E CABE A VOCÊ RESOLVÊ-LO. :P   "
                print_danger "                                                               "
                print_info ""
            fi
		else
			print_info ""
			print_info "Realiza o merge de um branch no branch atual"
			print_info ""
			print_info "	Uso: gitmerge nome_do_branch [mensagem]"
			print_info ""
			print_info "	nome_do_branch: nome do branch que será jogado ao branch atual"
			print_info ""
			print_info "	mensagem -opcional: adiciona uma mensagem ao commit do merge"
			print_info ""
	fi
}

# Realiza push para todos os remotes
# Parâmetros
# 1 - Nome do branch
gitpush() {
    branchName=''
	if [ $# -gt 0 ]
		then
			branchName=$1
		else
			branchName=$(git rev-parse --abbrev-ref HEAD)
	fi

	for repo in $(git remote)
    do
        print_info ${repo}
        print_warning "Push para $repo"
        git push --tags --set-upstream ${repo} refs/heads/${branchName}:refs/heads/${branchName}
    done
}

# Remove um branch local e todos os branchs remotos com o mesmo nome
# Parâmetros
# 1 - Nome do branch
gitremovebranches() {
	if [ $# -gt 0 ]
		then
			branchName=$1
			print_danger "O branch '$branchName' será removido do seu computador e todos os servidores, deseja continuar (y/n)? "
			read answer
			if echo "$answer" | grep -iq "^y" ;then
                for repo in $(git remote)
                do
                    print_warning "Removendo branch $branchName do $repo"
                    git branch -d -r ${repo}/${branchName}
                    git push ${repo} :${branchName}
                done
			else
			    print_danger "Ação cancelada"
			fi
		else
			print_info ""
			print_info "Remove um branch local e seus remotos"
			print_info ""
			print_info "	Uso: gitremovebranches nome_do_branch"
			print_info ""
			print_info "	nome_do_branch: nome do branch que será aniquilado"
			print_info ""
	fi	
}

# Remove um branch local
# Obs: Os branchs remotos permanecerão inalterados.
# Parâmetros
# 1 - Nome do branch
gitremove() {
	if [ $# -gt 0 ]
		then
			branchName=$1
			if [ $(git rev-parse --abbrev-ref HEAD) == ${branchName} ]
				then
					print_warning "O branch atual é o mesmo que você quer apagar, por favor dê checkout para outro branch"
					print_warning "Para qual branch deseja mover após apagar esse branch? (Ex.: PA)"
                    read answer
                    if [ "$answer" != "" ];then
                        git checkout ${answer}
                        git branch -d ${branchName}
                    else
                        print_info "Ação cancelada"
                    fi
				else
					print_danger "O branch '$branchName' será removido do seu computador todos os commits não enviados serão perdidos, deseja continuar (y/n)? "
					read answer
					if echo "$answer" | grep -iq "^y" ;then
					    print_info "Removendo branch $branchName"
						git branch -d ${branchName}
					else
					    print_info "Ação cancelada"
					fi
			fi
		else
			print_info ""
			print_info "Remove um branch local"
			print_info "Obs: Os branchs remotos permanecerão inalterados."
			print_info ""
			print_info "	Uso: gitremove nome_do_branch"
			print_info ""
			print_info "	nome_do_branch: nome do branch que será aniquilado"
			print_info ""
	fi	
}

# Isola um branch local e remove todos os outros
# Obs: Os branchs remotos permanecerão inalterados.
# Parâmetros
# 1 - Nome do branch
gitisolate() {
	if [ $# -gt 0 ]
		then
			branchName=$1
			print_info "O branch '$branchName' será mantido do seu computador e todos os outros serão apagados, deseja continuar (y/n)? "
			read answer
			if echo "$answer" | grep -iq "^y" ;then
			  	git checkout ${branchName}
				git pull
				branchList=$(git branch)
				while IFS='' read -r line || [[ -n "$line" ]]; do
					if [[ ${line} != \** ]]; then
						gitremove $line
					fi	
				done
			else
			    print_info "Ação cancelada"
			fi
		else
			print_info ""
			print_info "Isola um branch local e remove todos os outros"
			print_info "Obs: Os branchs remotos permanecerão inalterados."
			print_info ""
			print_info "	Uso: gitisolate nome_do_branch"
			print_info ""
			print_info "	nome_do_branch: nome do branch que será mantido"
			print_info ""
	fi
}

#Serve para alterar a última mensagem de commit
gitlastmessage(){
    git commit --amend
}

#Rotina de trabalho para criação de aplicação para produção
gitapply(){

#   apagar branch candidato local
#   criar branch candidato local fazendo track
#   checkout para candidato
#   pull candidato
#   checkout master
#   pull master
#   merge candidato no master
#   push master
#   cria tag
#   push tag
#   deleta candidato

    masterBranch=$1
    candidateBranch=$2

    print_warning "Movendo para branch $masterBranch"
    git checkout masterBranch
    print_warning "Removendo Branch candidato local - $candidateBranch"
    gitremove ${candidateBranch}

    for repo in $(git remote)
    do
        print_warning "Trazendo nova cópia do branch candidato - $candidateBranch de $repo"
        git checkout -b ${candidateBranch} --track ${repo}/${candidateBranch}
        break
    done
    print_warning "Checkout de $candidateBranch"
    git checkout ${candidateBranch}
    print_warning "Pull de $candidateBranch"
    git pull
    print_warning "Checkout de $masterBranch"
    git checkout ${masterBranch}
    print_warning "Pull de $masterBranch"
    git pull
    print_warning "Merge do branch $candidateBranch em $masterBranch"
	git merge --no-ff --log ${candidateBranch}
    print_warning "Merge do branch $candidateBranch em $masterBranch"

	for repo in $(git remote)
    do
        print_warning "Enviando $masterbranch para $repo"
        git push --tags ${repo}
    done

    print_warning "Criando tag archive/$candidateBranch"
    git tag -a archive/$candidateBranch

    for repo in $(git remote)
    do
        print_warning "Enviando tags de $masterbranch para $repo"
        git push --tags ${repo}
        git push -v ${repo} refs/tags/archive/${candidateBranch}
    done

    print_warning "Deletando branch $candidateBranch local"
	git branch -d ${candidateBranch}

	for repo in $(git remote)
    do
        print_warning "Enviando $masterbranch para $repo"
        git branch -D -r ${repo}/${candidateBranch}
	    git push ${repo} :${candidateBranch}
    done

}

processGitLog() {
	if [ $# -gt 0 ]
		then
			folder="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" 
			folder=`echo $folder | tr " " "\\ "`
			scriptFile="$folder/processGitLog.groovy"
			groovy "$scriptFile" $1 $2
		else
			print_info ""
			print_info "Lista os commit's realizados no branch atual após um commit de referência"
			print_info ""
			print_info "	Uso: processGitLog pasta hash_commit"
			print_info ""
			print_info "	pasta: local onde o projeto está localizado"
			print_info "	hash_commit: hash referente ao commit de referência"
			print_info ""
			print_info "	Exemplo: processGitLog ~/Dropbox/IntelliJIDEAProjects/e-hub/PA_EXTRA/ePlataformaBasket ebf91af51f2939e54cfaa49b6328a363bcbb1d05"
			print_info ""
	fi	


}



