#!/usr/bin/env bash
#	----------------------------------------------------------------------------------------------
#	GENERAL EXPORTS AND CONFIGS
#	----------------------------------------------------------------------------------------------	
	alias ls='ls -ltrha'						#ls:			List files
	alias f='open -a Finder ./'                 # f:            Opens current directory in MacOS Finder
	alias qfind='find . -name '                 # qfind:    	Quickly search for file
	alias rm='rm -v'							#rm:			Remove files and folders verbose
	alias mv='mv -v'							#mv:			Move files and folders verbose
	alias b='cd ..'								# b:			Move back
	alias killjava='killall -9 java'			#killjava:		Kill all java apps


#   lr:  Full Recursive Directory Listing
#   ------------------------------------------
	alias lr='ls -R | grep ":$" | sed -e '\''s/:$//'\'' -e '\''s/[^-][^\/]*\//--/g'\'' -e '\''s/^/   /'\'' -e '\''s/-/|/'\'' | less'


#   cpuHogs:  Find CPU hogs
#   -----------------------------------------------------
    alias cpu_hogs='ps wwaxr -o pid,stat,%cpu,time,command | head -10'
