import groovy.json.JsonSlurper

public class ProcessGitLog {
    static String username
    static String password

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static void main(String[] args) {

        def console = System.console()

 //       username = console.readLine('> JIRA user: ')
 //       password = new String(console.readPassword('> JIRA password: '))
        process(args[0], args[1])
    }

    private static String returnSDP(String code) {
        return ""
        String sdp
        System.setProperty("proxySet", "true");
        System.setProperty("proxyHost", "10.128.131.16");
        System.setProperty("proxyPort", "3128");

        try {
            print "Buscando pelo código >${code}< no JIRA..."
            String authStringEnc = "${username}:${password}".getBytes().encodeBase64().toString()
            URL url = new URL("https://lgkaiser.atlassian.net/rest/api/2/search?jql=issuekey%3D${code}&fields=customfield_10005");
            URLConnection urlConnection = url.openConnection();
            urlConnection.setConnectTimeout(10000); // 10 segundos de timeout
            urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
            InputStream is = urlConnection.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);

            int numCharsRead;
            char[] charArray = new char[1024];
            StringBuffer sb = new StringBuffer();
            while ((numCharsRead = isr.read(charArray)) > 0) {
                sb.append(charArray, 0, numCharsRead);
            }
            String result = sb.toString();

            def jsonObj = new JsonSlurper().parseText(result)
            sdp = jsonObj.issues[0].fields.customfield_10005
            println " --> SDP ${sdp}"
        } catch(SocketTimeoutException ste) {
            System.out.println("Tempo limite de 10s ultrapassado");
        } catch (Exception e) {
            sdp = 'Erro ao recuperar SDP!!!'
            println " --> ${sdp}"
        }

        return sdp
    }

    private static void process(String path, String initialHash) {
        String command = "git --git-dir ${path}/.git status"

        // Considera o nome do projeto como sendo o último nível do path
        String project = path.split('/')[-1]
        // Obtem a primeira linha do git log
        String header = "${project} [${command.execute().text.split('\n')[0].substring(3)}]"

        command = "git --git-dir ${path}/.git log --pretty=oneline HEAD ^${initialHash}~0"
        List<String> lines = command.execute().text.split('\n')

        List<GitResult> gitResults = new ArrayList<GitResult>()

        for (String line in lines) {
            String hash = line.substring(0,40)

            if (hash == initialHash) {
//                result << "Versão em produção:[${getCommitDate(path, initialHash)}] [$hash] - $comment"
//                break
            }

            GitResult gitResult = new GitResult()
            gitResult.date = getCommitDate(path, hash)
            gitResult.hash = hash
            gitResult.message = line.substring(41)
            // Se iniciar por * ou Merge branch, indica que é uma linha de merge
            gitResult.merge = gitResult.message.indexOf('*') == 0 || gitResult.message.toUpperCase().indexOf('MERGE') == 0
            if (!gitResult.merge) {
                gitResult.code = gitResult.message.split(' ')[0]
            }

            gitResults << gitResult

        }

        consolidate(gitResults)

        println "-" * 80
        println header
        println eFabrikaFormat(gitResults)
        List<String> scripts = getScriptFiles(path, initialHash)
        if (scripts) {
            println "\n*** Scripts:\n\t${ANSI_RED}${scripts.join('\n\t')}${ANSI_RESET}"
        }
        println "-" * 80
        println header
        println npcFormat(gitResults)

        println "-" * 80
        GitResult prodResult = gitResults[0]
        println "${ANSI_RED}Informações para produção: [Data: ${prodResult.date}, hash: ${prodResult.hash}]${ANSI_RESET}"
        println "-" * 80
    }

    private static void consolidate(List<GitResult> gitResults) {
        Map<String> mpCodeSDP= new HashMap<String>()
        gitResults.code.findAll {it != null}.each {
            mpCodeSDP[it] = returnSDP(it)
        }
        gitResults.each {
            it.sdp = mpCodeSDP[it.code]
        }
    }

    private static String eFabrikaFormat(List<GitResult> gitResults) {
        List<String> results = new ArrayList<String>()
        gitResults.reverse().each {
            results << "\t${ANSI_GREEN}[${it.date}]${ANSI_YELLOW}[${it.hash.substring(0,7)}] ${ANSI_WHITE}- ${it.message.replace('   ','\n\t\t\t\t    ')}${ANSI_RESET}"
        }
        return results.join("\n")
    }

    private static String npcFormat(List<GitResult> gitResults) {
        List<String> results = new ArrayList<String>()
        gitResults.reverse().each {
            if (!it.merge) {
                results << "\t${ANSI_GREEN}[${it.date.substring(0,8)}]${ANSI_YELLOW}[${it.hash.substring(0,7)}] ${ANSI_WHITE}- ${it.message.replace('   ','\n\t\t\t\t    ')}${ANSI_RESET}"
            }
        }
        return results.join("\n")
    }

    private static String getCommitDate(String path, String hash) {
        String command = "git --git-dir ${path}/.git show --pretty=oneline --date=iso8601 --format=%ad ${hash}"
        String commitInfo = command.execute().text.split('\n')[0]
        String year = commitInfo.substring(2,4)
        String month = commitInfo.substring(5,7)
        String day = commitInfo.substring(8,10)
        String hour = commitInfo.substring(11,16)
        return "${day}/${month}/${year} ${hour}"
    }

    private static List<String> getScriptFiles(String path, String hash) {
        String command = "git --git-dir ${path}/.git diff --name-only HEAD ${hash}"
        List<String> files = command.execute().text.split('\n')
        return files.findAll{it.toLowerCase().indexOf('.sql') > 0}
    }
}

class GitResult {
    String date, hash, message, code, sdp
    Boolean merge = false
}
