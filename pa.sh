#!/usr/bin/env bash

connectPAHlgAws() {
    echo "fazendo ssh para servidor 10.250.193.232"
	ssh $USER_GPA@10.250.193.232
}

connectPAPrdAws() {
	echo "fazendo ssh para servidor 10.250.3.148"
	ssh $USER_GPA@10.250.3.148
}

kubePortForwardRedisHlg() {
	while :; do
		echo "Executando: kubectl -n data-store port-forward redis-data-store-redis-5c9666b48d-bckzp 6379:6379"
		kubectl -n data-store port-forward redis-data-store-redis-5c9666b48d-bckzp 6379:6379
	done
}

kubePortForwardRabbitHlg() {
	while :; do
		echo "Executando: kubectl -n data-store port-forward rabbitmq-data-store-rabbitmq-ha-0 5672:5672"
		kubectl -n data-store port-forward rabbitmq-data-store-rabbitmq-ha-0 5672:5672
	done
}

kubePortForwardMongoHlg() {
	replicaset=$1
    if [ "$replicaset" = "" ]; then
		replicaset=0
	fi
	while :; do
		echo "Executando: kubectl -n data-store port-forward mongodb-data-store-mongodb-replicaset-$replicaset 27017:27017"
		kubectl -n data-store port-forward mongodb-data-store-mongodb-replicaset-$replicaset 27017:27017
	done
}

kubePortForwardMongoPrd() {
	replicaset=$1
    if [ "$replicaset" = "" ]; then
		replicaset=0
	fi
	while :; do
		echo "Executando: kubectl -n prod port-forward mongodb-prod-v2-mongodb-replicaset-$replicaset 27017:27017"
		kubectl -n prod port-forward mongodb-prod-v2-mongodb-replicaset-$replicaset 27017:27017
	done
}

kubePortForwardSolrPrd() {
	while :; do
		echo "Executando: kubectl -n prod port-forward solr8-0 8983:8983"
		kubectl -n prod port-forward solr8-0 8983:8983
	done
}

kubePortForwardSolrHlg() {
	while :; do
		echo "Executando: kubectl -n data-store port-forward solr8-0 8983:8983"
		kubectl -n data-store port-forward solr8-0 8983:8983
	done
}

kubeContextHlg() {
	kubectl config use-context $USER_GPA@hlg.k8s.ecom.gpa.digital
	export NAMESPACEGPA=hlg
}

kubeContextDev() {
	kubectl config use-context $USER_GPA@hlg.k8s.ecom.gpa.digital
	export NAMESPACEGPA=dev2
}

kubeContextPrd() {
	kubectl config use-context $USER_GPA@prd.k8s.ecom.gpa.digital
	export NAMESPACEGPA=prod
}

kubeContextMktp() {
	kubectl config use-context $USER_GPA@mktplace-stage.k8s.ecom.gpa.digital
	export NAMESPACEGPA=prod
	echo $NAMESPACEGPA
}

kubeGetContextFormated() {
    echo "-------------------"
    echo "Usando contexto `kubeGetContext`"
    echo "-------------------"
}

kubeGetContext() {
	kubectlctx=`kubectl config current-context`
	kubectlctx=${kubectlctx#*@}
    if [ "$kubectlctx" = "prd.k8s.ecom.gpa.digital" ]; then
       kubectlctx="prd"
    fi
    if [ "$kubectlctx" = "hlg.k8s.ecom.gpa.digital" ]; then
       kubectlctx="hlg"
    fi
	echo $kubectlctx
}

kubeGetPods() {
	kubectl get po -n $NAMESPACEGPA | grep $1
}

kubeLogPod() {
	kubectl logs -n $NAMESPACEGPA -f $1
}

kubeLogService() { 
	numLines=$2
    if [ "$numLines" = "" ]; then
		numLines=10
	fi
	echo $numLines
	kubectl logs -f --tail=$numLines --max-log-requests=15 -n $NAMESPACEGPA -l app=$1
}

# Faz port-forward para um serviço específico
# Uso: kubePortForward <pod> <porta local>:<porta remota>
# Exemplo: kubePortForward checkout-site-job 8585:8080
kubePortForward() {
  [ -z "$NAMESPACEGPA" ] && echo "Contexto não configurado!" && return 0
  [ -z "$2" ] && echo "Uso: kubePortForward <pod> <porta local>:<porta remota>" && return 0
  echo "Buscando pod com o serviço $1 com 1/1..."
  pod=$(kubeGetPods $1 | grep 1/1 | head -n 1 | cut -d " " -f1)
  [ -z "$pod" ] && echo "Pod não encontrado!" && return 0
  echo "Pod encontrado: $pod"
  ports=$2
  while :; do
    echo "Executando: kubectl -n $NAMESPACEGPA port-forward $pod $ports"
    kubectl -n $NAMESPACEGPA port-forward $pod $ports
  done
}